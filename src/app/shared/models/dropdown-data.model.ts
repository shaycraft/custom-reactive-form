import { DropdownItem } from './dropdown-item.model';

interface DropdownDataInterface {
  selectVerbiage?: string;
  list: DropdownItem[];
}

export class DropdownData implements DropdownDataInterface {
  public selectVerbiage?: string | undefined;
  public list: DropdownItem[];

  constructor({ list, selectVerbiage }: DropdownDataInterface) {
    this.list = list;
    this.selectVerbiage = selectVerbiage;
  }
}
