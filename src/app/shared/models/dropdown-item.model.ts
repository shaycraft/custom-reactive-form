interface DropdownItemInterface {
  id?: number | string;
  description?: string;
}

export class DropdownItem implements DropdownItemInterface {
  public id?: number | string;
  public description?: string;

  constructor({ id, description }: DropdownItemInterface = {}) {
    this.id = id;
    this.description = description;
  }
}
