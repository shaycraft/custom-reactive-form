// angular
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DropdownData } from '../models/dropdown-data.model';

import { DropdownItem } from '../models/dropdown-item.model';

@Component({
  selector: 'app-custom-dropdown',
  templateUrl: './custom-dropdown.component.html',
  styleUrls: ['./custom-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomDropdownComponent),
      multi: true,
    },
  ],
})
export class CustomDropdownComponent implements ControlValueAccessor {
  // public input properties

  @Input() public data: DropdownData | null = null;

  // public properties

  public onChanged: (id: number | string | undefined) => void = () => {};
  public onTouched: () => void = () => {};
  public selectedId?: number | string;

  constructor(private readonly _cdr: ChangeDetectorRef) {}

  // events

  public onChange(id: string): void {
    if (!id) {
      this.selectedId = undefined;
    } else if (isNaN(+id)) {
      this.selectedId = id;
    } else {
      this.selectedId = +id;
    }

    this.onChanged(this.selectedId);
    this.onTouched();
  }

  // public methods

  public writeValue(val: number | string): void {
    if (val !== this.selectedId) {
      this.selectedId = val;
      this._cdr.detectChanges();
    }
  }

  public registerOnChange(fn: any): void {
    this.onChanged = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
