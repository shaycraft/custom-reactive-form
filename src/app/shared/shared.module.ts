// angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomDropdownComponent } from './components/custom-dropdown.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CustomDropdownComponent],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [
    // modules
    CommonModule,
    ReactiveFormsModule,
    CustomDropdownComponent,
  ],
})
export class SharedModule {}
