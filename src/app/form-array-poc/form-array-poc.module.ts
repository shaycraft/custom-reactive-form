import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormContainerComponent } from './components/form-container/form-container.component';
import { SharedModule } from '../shared/shared.module';
import { FormsArrayPocRoutingModule } from './form-aray-poc-routing.module';

@NgModule({
  declarations: [FormContainerComponent],
  imports: [CommonModule, SharedModule, FormsArrayPocRoutingModule],
})
export class FormArrayPocModule {}
