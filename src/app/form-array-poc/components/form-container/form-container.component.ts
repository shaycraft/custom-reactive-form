// angular
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';

// rxjs
import { merge, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-form-container',
  templateUrl: './form-container.component.html',
  styleUrls: ['./form-container.component.scss'],
})
export class FormContainerComponent implements OnInit, OnDestroy {
  public namesList: string[] = ['Fred', 'Alice', 'Suzy'];
  public namesForm: FormGroup;
  public get namesListControl(): FormGroup[] {
    return (this.namesForm.get('namesList') as FormArray)
      .controls as FormGroup[];
  }

  private _unsubscribe$: Subject<void> = new Subject<void>();

  constructor(private _fb: FormBuilder) {
    this.namesForm = this._fb.group({
      namesList: this.getNamesFormArray(),
    });

    this._unsubscribe$.subscribe(() => {});
  }

  public ngOnInit(): void {
    const formArray = this.namesForm.get('namesList') as FormArray;

    merge(...formArray.controls.map((control) => control.valueChanges))
      .pipe(takeUntil(this._unsubscribe$))
      .subscribe((val) => {
        console.debug('Change triggered by', val);
      });

    this.namesForm.valueChanges
      .pipe(takeUntil(this._unsubscribe$))
      .subscribe((val) => {
        console.debug('form Changes = ', val);

        const fa = this.namesForm.get('namesList') as FormArray;
        console.log(
          'dirty =',
          fa.controls.filter((c) => c.dirty)
        );
      });
  }

  public ngOnDestroy(): void {
    if (this._unsubscribe$) {
      this._unsubscribe$.next();
      this._unsubscribe$.complete();
    }
  }

  public getNamesFormArray(): FormArray {
    const fa: FormArray = new FormArray([]);
    this.namesList.forEach((name, i) =>
      fa.push(
        new FormGroup({
          id: new FormControl(`${name}_${i}`),
          name: new FormControl(name),
          selected: new FormControl(false),
        })
      )
    );

    return fa;
  }
}
