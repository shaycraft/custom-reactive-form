// models
import { Country } from '../models/country.model';
import { State } from '../models/state.model';
import { statesUS } from './state-list.constants';

export const countries: Country[] = [
  { id: 1, name: 'USA', states: mapCountryStates(statesUS) },
  { id: 2, name: 'Japan' },
  { id: 3, name: 'Canada' },
  { id: 4, name: 'Brazil' },
  { id: 5, name: 'Mexico' },
  { id: 6, name: 'Germany' },
  { id: 7, name: 'Russia' },
  { id: 8, name: 'India' },
  { id: 9, name: 'Egypt' },
  { id: 10, name: 'Nigeria' },
];

function mapCountryStates(states: { [key: string]: string }): State[] {
  return Object.keys(states).map(
    (key) => new State({ code: key, name: states[key] })
  );
}
