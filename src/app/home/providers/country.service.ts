// angular
import { Injectable } from '@angular/core';

// rxjs
import { Observable, of } from 'rxjs';

// constants
import { countries } from '../constants/country-list.constants';

// models
import { Country } from '../models/country.model';
import { State } from '../models/state.model';

@Injectable({
  providedIn: 'root',
})
// TODO: rename to `GeographyService`
export class CountryService {
  public getCountries(): Observable<Country[]> {
    return of(
      countries.map((country) => ({ id: country.id, name: country.name }))
    );
  }

  public getStates(countryId: number): Observable<State[]> {
    return of(
      countries.find((country) => country.id === countryId)?.states as State[]
    );
  }
}
