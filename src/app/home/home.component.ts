// angular
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

// rxjs
import { EMPTY, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

// shared
import { DropdownData } from '../shared/models/dropdown-data.model';
import { DropdownItem } from '../shared/models/dropdown-item.model';

// models
import { Country } from './models/country.model';
import { State } from './models/state.model';

// services
import { CountryService } from './providers/country.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class HomeComponent implements OnInit, OnDestroy {
  // public properties

  public readonly testForm: FormGroup;
  public countries$: Observable<Country[]> = EMPTY;
  public states$: Observable<State[]> = EMPTY;

  // private properties

  private readonly _fb: FormBuilder = new FormBuilder();
  private readonly _unsubscribe: Subject<void> = new Subject<void>();

  constructor(private readonly _countryService: CountryService) {
    this.testForm = this.initializeTestForm();
  }

  public ngOnInit(): void {
    this.countries$ = this._countryService.getCountries();
    (this.testForm.get('country') as FormControl).valueChanges
      .pipe(takeUntil(this._unsubscribe))
      .subscribe((countryId: number) => {
        this.testForm.get('state')?.reset();
        this.states$ = this._countryService.getStates(countryId);
      });

    this.testForm.valueChanges.subscribe((val) => console.debug(val));
  }

  public ngOnDestroy(): void {
    if (this._unsubscribe) {
      this._unsubscribe.next();
      this._unsubscribe.complete();
    }
  }

  /**
   * resets form when reset button clicked
   */
  public resetForm(): void {
    this.testForm.reset();
  }

  // public methods

  /**
   *
   * @param countries
   * @returns data for county dropdown
   */
  public getCountryDropdownViewModel(countries: Country[]): DropdownData {
    return {
      list: this.getCountries(countries),
      selectVerbiage: '--- Select a Country ---',
    };
  }

  public getStateDropdownViewModel(states: State[]): DropdownData {
    return {
      list: this.getStates(states),
      selectVerbiage: '--- Select a State ---',
    };
  }

  // private methods

  private initializeTestForm(): FormGroup {
    return this._fb.group({
      foo: this._fb.control('bar', [Validators.required]),
      country: this._fb.control(null, []),
      state: this._fb.control(null, []),
    });
  }

  private getCountries(countries: Country[]): DropdownItem[] {
    return countries.map((country: Country) => {
      return {
        id: country.id,
        description: country.name,
      };
    });
  }

  private getStates(states: State[]): DropdownItem[] {
    return states.map((state: State) => {
      return {
        id: state.code,
        description: state.name,
      };
    });
  }
}
