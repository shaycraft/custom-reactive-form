import { State } from './state.model';

interface CountryInterface {
  id: number;
  name: string;
  states?: State[];
}

export class Country implements CountryInterface {
  id: number;
  name: string;
  states?: State[];

  constructor({ id, name, states }: CountryInterface) {
    this.id = id;
    this.name = name;
    this.states = states;
  }
}
