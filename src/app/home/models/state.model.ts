interface StateInterface {
  code: string;
  name: string;
}

export class State implements StateInterface {
  public code: string;
  public name: string;

  constructor({ code, name }: StateInterface) {
    this.code = code;
    this.name = name;
  }
}
